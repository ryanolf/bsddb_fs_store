
"""
Berkeley DB - HDF5 store hybrid
=================
This file defines a hybrid Berkeley DB/filesystem store.  This stores
data files in a specified directory in a filesystem as hdf5 and metadata
in a Berkeley DB database.  Data files are stored in files with name
key+'.hdf5'. This class is based on the AbstractDataStore class from
Enthought, but a key difference is that data is passed in a dict instead
file-like. This is better behavior for our application: HDF5 file
objects are not really "file-like", and they interface with numpy arrays
directly very well.
"""

# System library imports.
import os
import cPickle
from bsddb3 import db
import h5py
import numpy as np
from contextlib import contextmanager
from collections import Iterable, MutableSequence, MutableSet, \
    Mapping, MutableMapping
from base64 import urlsafe_b64encode, urlsafe_b64decode

from weakref import WeakSet

MutableMapping.register(h5py.File)
MutableMapping.register(h5py.Group)

class DataStore(object):
    """Abstract base class of Data Stores
    
    Right now there is only DBFileSystemStore, but in principle other
    stores could implement the same interface. They should inherit from
    this class. This class could also provide skeleton methods and other
    documentation for generic DataStores. It could even turn into an
    Interface (as defined by Enthought.)
    """
    pass

class DBFileSystemStoreError(Exception):
    """Generic filesystem store error"""
    pass

class DBFileSystemLockingError(DBFileSystemStoreError):
    """Cursor is locked in presence of """

class DBFileSystemIndexError(DBFileSystemStoreError):
    """Filesystem store secondary index error"""
    pass

def init_store(path, magic_fname='.DBFSStore'):
    """Create the magic file for the store.  Useful to initialize
    the store for the first time.
    
    Parameters
    ----------
    path :
        The directory that will be used for the file store.
    magic_fname :
        The name of the magic file in that directory,
    """
    magic_path = os.path.join(path, magic_fname)
    with open(magic_path, 'wb') as magic_fp:
        magic_fp.write('__version__ = 0\n')

class DBFileSystemEnvironment(object):
    """Berkeley DB Environment class for transactions etc"""

    def __init__(self, path, recover=False):
        """Set recover = True to rebuild the database environment

        It is important that no other objects are referencing the
        environment during recovery"""
        self._dbEnv_open_flags = db.DB_CREATE | db.DB_INIT_MPOOL | \
          db.DB_INIT_TXN | db.DB_INIT_LOG | db.DB_INIT_LOCK
        self._dbEnv_flags = db.DB_TXN_NOWAIT
        if recover:
            self._dbEnv_open_flags |= db.DB_RECOVER
        self._dbEnv = db.DBEnv()
        #self._dbEnv.set_lk_detect(db.DB_LOCK_OLDEST)
        self._dbEnv.open(path,self._dbEnv_open_flags)
        self._is_open = True
        self._dbEnv.set_flags(self._dbEnv_flags, 1)
        # use a WeakSet to store references to dbstore objects to
        # allow garbage collection. These references won't stop these
        # dbstores from being garbage collected, avoiding circuilar
        # references that can cause dbstore and dbenvs to live
        # forever!!
        self._dbStoreHandles = WeakSet()

    def register(self, datastore):
        """Keep track of database objects so we can close them"""
        self._dbStoreHandles.add(datastore)

    def unregister(self, datastore):
        try:
            self._dbStoreHandles.remove(datastore)
        except KeyError:
            pass

    def close(self):
        for dbstore in self._dbStoreHandles:
            # explicitly close any data stores to enable proper
            # cleanup of cursors and the like, even though closing
            # dbEnv closes databases
            dbstore.disconnect(force=True)
        self._dbEnv.close()
        self._is_open = False

    def __del__(self):
        self.close()

    @property
    def is_open(self):
        return self._is_open

    @property
    def dbEnv(self):
        return self._dbEnv

################################################################################
# SharedFSStore class.
################################################################################
class DBFileSystemStore(DataStore):
    """
    A store that uses a Shared file system to store the data, BSDDB for metadata.
    """
    def __init__(self, path, db_file_name, db_env_object=None, \
                 magic_fname='.DBFSStore', debug=False):
        """Initializes the store given a path to a store. 
        
        Parameters
        ----------        
        path : str:
            A path to the root of the file system store.
        magic_fname :
            The name of the magic file in that directory,

        """

        # Turn debug messages off by default
        self._debugmessages = debug
        
        self._root = path
        self._data_path = os.path.join(path, db_file_name + '_data')
        self._magic_fname = magic_fname
        self._connected = False
        
        if not os.path.exists(path):
            raise DBFileSystemStoreError('Unable to find path %s'%path)
        # The path should have a .DBFSStore file.
        if not (os.path.exists(os.path.join(path, self._magic_fname))):
            raise DBFileSystemStoreError('Path %s is not a valid store'%path)

        if not os.path.exists(self._data_path):
            os.mkdir(self._data_path)
        # Initialize the db variables and instances
        # We will operate a DBEnv because we want to use transactions
        # (DB_AUTO_COMMIT on the DB)
        if db_file_name is None:
            raise DBFileSystemStoreError('no database name specified')

        self._db_name = db_file_name
        if db_env_object:
            # db file will be in environment
            self._db_path = db_file_name
            # enable transactions with AUTO_COMMIT
            self._db_flags = db.DB_AUTO_COMMIT | db.DB_CREATE | db.DB_MULTIVERSION
            self._db2_create_flags = self._db_flags | db.DB_EXCL
            self._db2_open_flags = db.DB_AUTO_COMMIT | db.DB_MULTIVERSION
            self._dbEnv_object = db_env_object
            self._dbEnv = db_env_object.dbEnv
            self._dbEnv_removedb_flags = db.DB_AUTO_COMMIT
            self._dbEnv_object.register(self)
        else:
            self._db_path = os.path.join(path, db_file_name)
            self._db_flags = db.DB_CREATE
            self._db2_create_flags = self._db_flags | db.DB_EXCL
            self._db2_open_flags = 0
            self._dbEnv = None
        self._db_type = db.DB_BTREE
        self._db2_handle_flags = db.DB_DUPSORT   # flags for index database handles
        self._db_info_fname = \
          os.path.join(self._root, db_file_name + ".dbinfo")
        if not os.path.exists(self._db_info_fname):
            self._db_info = {}
            self._db_info_update()
        else:
            with open(self._db_info_fname,'rb') as f:
                self._db_info = cPickle.load(f)

        # We need to keep track of open cursors, e.g. querys in progress.
        # Without a dbEnv, cursors need to lock out writes.
        # Even with a dbEnv, it's best not to close the databases
        # with open cursors. In this set we'll store unique handles to 
        self._cursorLock = set()
        self._db_cursor_flags = db.DB_READ_COMMITTED
        self._db2_cursor_flags = db.DB_READ_COMMITTED
        # Also keep track of open transactions for graceful aborting
        self._txnSet = set()
        # HDF5 options: enable compression on datasets
        self._hdf_dset_args = {'compression' : 'lzf', 'shuffle' : True}
        self._hdf_dset_args_scalar = {} # scalar datasets don't support above

    def __del__(self):
        try:
            self.disconnect(force=True)
        except:
            raise
        finally:
            self._dbEnv_object.unregister(self)
        

    def connect(self,credentials=None):
        """ Connect to the key-value store by opening db and dbEnv
        
        Parameters
        ----------
        credentials : 
            These are not used by default.
            
        """

        if self._connected:
            return
        try:
            # You cannot open a db or dbEnv twice, so constructor must
            # be called every time
            self._db = db.DB(self._dbEnv) if self._dbEnv else db.DB()
            self._db.open(self._db_path, dbtype=self._db_type, flags=self._db_flags)
        except BaseException as err:  # roll back
            if hasattr(self,'_db'):
                self._db.close()
            raise err
        try:
        # We also need to open all secondary index DBs to keep them in sync
            self._db2 = {}
            for k,v in self._db_info['indices'].iteritems():
                # k is the metadata key string used for the index.
                # v is index file name
                if self._dbEnv:
                    indexdb = db.DB(self._dbEnv)
                else:
                    indexdb = db.DB()
                    v = os.path.join(self._root, v)
                indexdb.set_flags(self._db2_handle_flags)
                indexdb.open(v, dbtype=self._db_type, flags=self._db2_open_flags)
                self._db.associate(indexdb,
                            self._index_callback(self._idxkey_from_string(k)))
                self._db2[k] = indexdb
        except KeyError: # _db_info may not have indices in it
            pass
        except BaseException as err: # anything else, roll back everything
            for _db in self._db2.itervalues():
                _db.close()
            if self._db:
                self._db.close()
            raise err
        self._connected = True
                
    def disconnect(self, force=False):
        """Disconnect from the key-value store

        Close the db.  If force is True, we will disconnect even if we
        have open cursors. Otherwise, open cursors will raise and
        exception. Also if force is True, we'll attempt to close all
        databases even if we don't think they are open.
        """
        if self._cursorLock and not force:
            raise DBFileSystemLockingError(\
'Query in progress (open cursors). Cannot disconnect until query completes.')
        else:
            for cursor in self._cursorLock:
                try:
                    cursor.close()
                except Exception as err:
                    self._error('exception closing cursor: {}'.format(err))
            self._cursorLock.clear()
            for txn in self._txnSet:
                txn.abort()
        if self._connected or force:
            try:
                for k,indexdb in self._db2.iteritems():
                    self._debug('closing index for key "{}"...'.format(k))
                    indexdb.close()
                    self._debug('closed')
            except AttributeError:
                pass
            except Exception as err:  # even if for whatever reason these fail, we
                     # still want to close other things down
                self._error('exception closing index: {}'.format(err))
            try:
                self._debug('closing db')
                self._db.close()
            except AttributeError:
                pass
            except Exception as err:
                self._error('exception closing db: {}'.format(err))
            self._connected = False
    
    def create_index(self, mdkey):
        """Create an index on given metadata key

        In the index database, the mdkey's value (as a string) is the
        key, making lookups fast.  The mdkey can be a top-level metadata
        key to index, or a tuple. The tuple specifies a heirarchy of keys,
        allowing us to index on items below top level of the dictionary.
        """
    
        keystr = self._idxkey_to_string(mdkey)
        # add indices to our info file if it's not there
        if 'indices' not in self._db_info:
            self._db_info['indices'] = {}
        elif keystr in self._db_info['indices']:
            raise DBFileSystemIndexError('index already exists')
        ## First, construct the callback function
        callback = self._index_callback(mdkey)
        dbname = keystr  + "_" + self._db_name

        with self._dbcontext():
            if self._dbEnv:
                indexdb = db.DB(self._dbEnv)
                dbpath = dbname
            else:
                indexdb = db.DB()
                dbpath = os.path.join(self._root,  dbname)
            indexdb.set_flags(self._db2_handle_flags)
            indexdb.open(dbpath, dbtype=self._db_type, flags=self._db2_create_flags)
            self._db2[keystr] = indexdb
            # The following step could take some time: it has to pull
            # everything from the database and apply the callback
            self._db.associate(indexdb, callback, db.DB_CREATE)

        self._db_info['indices'][keystr] = dbname
        self._db_info_update()

    def get_indices(self):
        try:
            return (self._idxkey_from_string(k) for k
                    in self._db_info['indices'].viewkeys())
        except KeyError:
            pass
        return {}.iterkeys()
        
    def destroy_index(self, mdkey):
        """ Remove the index on mdkey """
        if self._cursorLock:
            raise DBFileSystemLockingError(\
'Query in progress (open cursors). Complete the query before destroying indices.')
        # use explicit construction for dbname in case it's not in _db_info
        keystr = self._idxkey_to_string(mdkey)
        dbname = keystr + "_" + self._db_name
        try:
            state = self._connected
            if state:
                self.disconnect()  # remove databases with all DBs closed
            if self._dbEnv:
                self._dbEnv.dbremove(dbname,flags=self._dbEnv_removedb_flags)
            else:
                dbhandle = db.DB()
                dbpath = os.path.join(self._root, dbname)
                dbhandle.remove(dbpath)
            if state:
                self.connect()
        except db.DBNoSuchFileError as err:
            self._error(err)
        try:
            del self._db_info['indices'][keystr]
            self._db_info_update()
        except KeyError:
            pass

    def is_connected(self):
        """ Whether or not the store is currently connected
        
        Returns
        -------
        connected : bool
            Whether or not the store is currently connected.

        """
        return self._connected
    
    def info(self):
        """ Get information about the key-value store
        
        Returns
        -------
        
        metadata : dict
            A dictionary of metadata giving information about the key-value store.
        """
        return {'type': 'DBFileSystemStore', 'version': 0}
    
    ##########################################################################
    # Basic Create/Read/Update/Delete Methods
    ##########################################################################
    def get(self, key, mdselect=None, dselect=None):
        """ Retrieve a stream of data and metdata from a given key in the key-value store.
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
        d/mdselect : see get_data and get_metadata select
        
        Returns
        -------
        data : file-like
            A readable file-like object that provides stream of data from the
            key-value store
        metadata : dictionary
            A dictionary of metadata for the key.
        
        Raises
        ------
        KeyError :
            If the key is not found in the store, a KeyError is raised.

        """
        data = self.get_data(key, dselect)
        metadata = self.get_metadata(key, mdselect)
        return (data, metadata)
    
    def set(self, key, value, buffer_size=1048576):
        """ Store a stream of data into a given key in the key-value store.
        
        This may be left unimplemented by subclasses that represent a read-only
        key-value store.
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
        value : tuple of file-like, dict
            A pair of objects, the first being a readable file-like object that
            provides stream of data from the key-value store.  The second is a
            dictionary of metadata for the key.
        buffer_size : int
            An optional indicator of the number of bytes to read at a time.
            Implementations are free to ignore this hint or use a different
            default if they need to.  The default is 1048576 bytes (1 MiB).
        
        """

        data, metadata = value
        self.set_data(key, data)
        self.set_metadata(key, metadata)
 
        
    
    def delete(self, key):
        """ Delete a key from the repsository.
        
        This may be left unimplemented by subclasses that represent a read-only
        key-value store.
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
            
        """
        if self._cursorLock and not self._dbEnv:
            raise DBFileSystemLockingError(\
'Query in progress (open cursors) and no dbEnv to protect transactions. '
'Either use a dbEnv, finish your queries, or execute your queries without '
'using generators.')
        try:
            with self._dbcontext() as _db:
                _db.delete(key)
        except db.DBNotFoundError:
            pass
        data_path = self._get_data_path(key)
        if os.path.exists(data_path):
            os.remove(data_path)
    
    def get_data(self, key, select=None):
        """ Retrieve a stream from a given key in the key-value store.
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.

        select : tree of keys
        
        Returns
        -------
        data : file-like
            A readable file-like object the that provides stream of data from the
            key-value store.
        
        Raises
        ------
        KeyError :
            This will raise a key error if the key is not present in the store.

        """
        data_path = self._get_data_path(key)
        if not self.exists(key):
            raise KeyError('Key %s does not exist in store!'%key)
        elif not os.path.exists(data_path):
            return None
        else:
            try:
                with h5py.File(data_path,'r') as f:
                    if select is None:
                        ds = f
                    else:
                        ds = _recursive_dict_get_subset(select, f)
                    data = _recursive_dict_map(np.array, ds) #as numpy
            except IOError as err:
                raise IOError(err)  # reraise to debug
                data = None
        return data
                
    
    def get_metadata(self, key, select=None):

        """ Retrieve the metadata for a given key in the key-value store.
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
        select : iterable of strings or None
            Which metadata keys to populate in the result.  If unspecified, then
            return the entire metadata dictionary.
        
        Returns
        -------
        metadata : dict
            A dictionary of metadata associated with the key.  The dictionary
            has keys as specified by the select argument.  If a key specified in
            select is not present in the metadata, then it will not be present
            in the returned value.
        
        Raises
        ------
        KeyError :
            This will raise a key error if the key is not present in the store.

        """
        with self._dbcontext() as _db:
            md = _db.get(key)
        if md is None:
            raise KeyError('Key %s does not exist in store!'%key)         
        md = self._unpack_metadata(md)
        if select is None:
            return md
        else:
            return dict((k, md[k]) for k in select if k in md)

    def update_data(self, key, data):
        """ Update data for given key in the key-value store

        Existing data is preserved if no key in parameter data matches it.
        """
        self.set_data(key, data, update_only = True)

    def set_data(self, key, data, update_only=False):
        """ Replace the data for a given key in the key-value store.
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
        data : a dict of string keys and numpy array values (or values castable)
        update-only: set to false to remove data that is stored but not in data
            dictionary

        """
        if self._cursorLock and not self._dbEnv:
            raise DBFileSystemLockingError(\
'Query in progress (open cursors) and no dbEnv to protect transactions. '
'Either use a dbEnv, finish your queries, or execute your queries without '
'using generators.')
        if data == None:  # should we delete any existing data? not for now.
            return
        data_path = self._get_data_path(key)
        
        with h5py.File(data_path) as f:
            newkeys = set(_recursive_dict_keys(data))
            oldkeys = set(_recursive_dict_keys(f))

            # delete data that is no longer in the set. we do this
            # first because free space may be reused while the file is
            # open. once closed, free space becomes a "hole" in the
            # file. See the hdf5 user manual (under "freespace
            # management") for more info. We can periodically
            # re-create the files and copy data if we find them
            # growing too large. Another solution would be just to
            # recreate the file everytime we set the data in this
            # function.
            if not update_only:
                for k in oldkeys - newkeys:
                    _recursive_dict_del(f,k)
            for k in newkeys & oldkeys: #update keys that are in both
                d = self._to_array(_recursive_dict_get(data,k))
                ds = _recursive_dict_get(f,k)
                ds_args = self._hdf_dset_args \
                  if d.shape != () else self._hdf_dset_args_scalar
                if ds.dtype == d.dtype \
                    and ds.shape == d.shape: # reuse dataset
                    ds[...] = d
                else:
                    _recursive_dict_del(f,k)
                    f.create_dataset(name='/'.join(k), data=d, **ds_args)
            for k in newkeys - oldkeys: # create keys that are new
                d = self._to_array(_recursive_dict_get(data,k))
                ds_args = self._hdf_dset_args \
                  if d.shape != () else self._hdf_dset_args_scalar
                f.create_dataset(name='/'.join(k), data=d, **ds_args)

    
    def set_metadata(self, key, metadata):
        """ Set new metadata for a given key in the key-value store.
        
        This replaces the existing metadata set for the key with a new set of
        metadata.
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
        metadata : dict
            A dictionary of metadata to associate with the key.  The dictionary
            keys should be strings which are valid Python identifiers.
            
        """
        if self._cursorLock and not self._dbEnv:
            raise DBFileSystemLockingError(\
'Query in progress (open cursors) and no dbEnv to protect transactions.'
'Either use a dbEnv, finish your queries, or execute your queries without'
'using generators.')
        metadata = self._pack_metadata(metadata)
        with self._dbcontext() as _db:
            _db.put(key,metadata)
            _db.sync()
        
    
    def update_metadata(self, key, metadata):
        """ Update the metadata for a given key in the key-value store.
        
        This performs a dictionary update on the existing metadata with the
        provided metadata keys and values
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
        metadata : dict
            A dictionary of metadata to associate with the key.  The dictionary
            keys should be strings which are valid Python identifiers.
        
        """
        md = self.get_metadata(key)
        md.update(metadata)
        self.set_metadata(key, metadata)
        
    
    def exists(self, key):
        """ Test whether or not a key exists in the key-value store
        
        Parameters
        ----------
        key : string
            The key for the resource in the key-value store.  They key is a unique
            identifier for the resource within the key-value store.
        
        Returns
        -------
        exists : bool
            Whether or not the key exists in the key-value store.
            
        """
        with self._dbcontext() as _db:
            return _db.exists(key)


    def query(self, select=None, **kwargs):
        """ Query for keys and metadata

        This provides a very simple querying interface that returns subset
        matches with the metadata.  If no arguments are supplied, the query
        will return the complete set of metadata for the key-value store.
        
        Parameters
        ----------
        select : iterable of strings or dictionaries 
            An optional (possibly multilevel) list of metadata keys to return.
b            If this is not None, then the metadata dictionaries will only have
            values for the specified keys populated.        
        kwargs :
            Arguments where the keywords are metadata keys, and values are
            possible values for that metadata item. If item is itself a
            a dictionary, search is recursive.

        Returns
        -------
        result : iterable in the form of a generator
            An iterable of (key, metadata) tuples where metadata matches
            all the specified values for the specified metadata keywords.
            If a key specified in select is not present in the metadata of a
            particular key, then it will not be present in the returned value.
        
        """
        ## In btree databases, ranges of keys (by default sorted
        ## lexicographically) can be specified for search, potentially
        ## speeding up unindexed searching, but this is not
        ## implemented here

        ## First, get the kwargs corresponding to indexed keys
        try:
            allkeys = _recursive_dict_keys(kwargs)
            allkeystrs = (self._idxkey_to_string(k) for k in allkeys)
            idxkeys = [(k,kstr) for k,kstr in zip(allkeys,allkeystrs) if
                       kstr in self._db_info['indices']]
        except KeyError:
            idxkeys = []
            
        with self._cursorcontext(idxkeys) as cursors:
            have_matches = []  # we'll store cursors with matches here
            for k,c in zip(idxkeys,cursors[1:]):   # remember k is a tuple
                val = _recursive_dict_pop(kwargs, k[0])
                if _is_map_or_mutable_iterable(val):
                    # This value is mutable iterable, so we check for subsets,
                    # not equality. Indexed keys with mutable iterable values
                    # have one entry in the index database for each
                    # value, so we have create cursors to look for
                    # each item in val
                    self._debug('value for {} is a mutable iterable'.format(k[1]))
                    for item in val:
                        self._debug('new cursor for {}'.format(item))
                        if c.set(str(item)):
                            # Create a duplicate cursor at the correct position
                            nc = c.dup(db.DB_POSITION)
                            # Add it to the cursors lists so it gets
                            # shutdown appropriately
                            cursors.append(nc)
                            self._cursorLock.add(nc)
                            self._debug('using cursor for {} '\
                                        'with {} matches\n'.format(item,nc.count()))
                            have_matches.append(nc)
                        else:
                            self._debug('cursor for {} had no match'.format(item))
                            return
                else:
                    # This value is NOT iterable, so just look for it     
                    self._debug('setting cursor for {} to {}'.format(k[1],val))
                    if c.set(str(val)):
                        self._debug('using cursor for {} '\
                                    'with {} matches\n'.format(k[1],c.count()))
                        have_matches.append(c)
                    else:
                        self._debug('cursor for {} '\
                                    'yielded no matches\n'.format(k[1]))
                        return
            with self._joincontext(have_matches) as cursor:
                if cursor is None: # there were no idxkeys
                    cursor = cursors[0] # use the primary cursor
                    get_next = cursor.next  # use the next method to iterate
                else:
                    get_next = cursor.get  # join cursor uses get to iterate
                try:
                    ## cycle through records and yield matches of
                    ## remaining keys
                    while True:
                        r = get_next(flags=0)
                        # we have a record, extract key, value
                        if r is not None:
                            k,v = r
                        # no more records
                        else:
                            break
                        md = self._unpack_metadata(v)
                        if not _recursive_dict_subset(kwargs, md):
                            continue
                        if select is None:
                            yield (k,md)
                        else:
                            yield (k, _recursive_dict_get_subset(select, md))
                except db.DBNotFoundError:
                    pass

    def query_keys(self, **kwargs):
        """ Query for keys matching metadata provided as keyword arguments
        
        This provides a very simple querying interface that returns precise
        matches with the metadata.  If no arguments are supplied, the query
        will return the complete set of keys for the key-value store.
        
        This is equivalent to ``dict(self.query(**kwargs)).keys()``, but potentially
        more efficiently implemented.
        
        Parameters
        ----------
        kwargs :
            Arguments where the keywords are metadata keys, and values are
            possible values for that metadata item.

        Returns
        -------
        result : iterable as generator
            An iterable of key-value store keys whose metadata matches all the
            specified values for the specified metadata keywords.
        
        """
        result = self.query(**kwargs)
        for k,_ in result:
            yield k

    ##########################################################################
    # Private methods
    ##########################################################################

    @staticmethod
    def _idxkey_to_string(idxkey):
        """Make a string suitable for describing indices """
        if isinstance(idxkey, tuple):
            # Let's just make sure none of the keys have '->' in them
            for k in idxkey:
                if '<_-_-_>' in k:
                    raise \
   DBFileSystemIndexError('key should not have "<_-_-_>" in it')
            return urlsafe_b64encode('<_-_-_>'.join(idxkey))
        elif isinstance(idxkey, basestring):
            if '<_-_-_>' in idxkey:
                raise \
   DBFileSystemIndexError('key should not have "<_-_-_>" in it')
            return urlsafe_b64encode(idxkey)
        else:
            raise DBFileSystemIndexError('index key must be string or tuple')

    @staticmethod
    def _idxkey_from_string(keystr):
        """ Construct the idx key (string or tuple) from the keystr"""
        idxkey = urlsafe_b64decode(keystr).split('<_-_-_>')
        if len(idxkey) == 1:
            return idxkey[0]
        else:
            return tuple(idxkey)
        
    def _get_data_path(self, key):
        return os.path.join(self._data_path, urlsafe_b64encode(key) + '.hdf5')
        
    def _unpack_metadata(self, metadata):
        if metadata is not None:
            return cPickle.loads(metadata)

    def _pack_metadata(self, metadata):
        return cPickle.dumps(metadata)

    def _to_array(self, arraylike):
        if type(arraylike) == np.ndarray:
            return arraylike
        else:
            return np.array(arraylike)

    def _db_info_update(self):
        with open(self._db_info_fname,'wb') as f:
            cPickle.dump(self._db_info, f)

    def _index_callback(self, mdkey):
        if isinstance(mdkey, basestring):
            mdkey = (mdkey,) #tuplify for _recursive_dict_get
            def f(k,v):
                try:
                    md = _recursive_dict_get(self._unpack_metadata(v),mdkey)
                    if _is_map_or_mutable_iterable(md):
                        # return muliple keys: one for each value in
                        # the iterable.  Run through set() to
                        # eliminate duplicates. Note that if md is a
                        # dict, this yields the keys
                        return map(str, set(md))
                    else:
                        return str(md)
                except Exception:
                    return db.DB_DONOTINDEX
            return f
    
    @contextmanager
    def _dbcontext(self):
        state = self._connected
        if not state:
            self.connect()
        try:
            yield self._db
        except Exception as err:
            self._error('exception in db contex: {}'.format(err))
            #raise
        finally:
            if not state:
                self._debug('disconnecting')
                self.disconnect()
                self._debug('disconnected')

    @contextmanager
    def _cursorcontext(self, keys = []):
        """ keys is the list of index cursors to yield

        Keys is a list of the form [(key, keystring), ...].
        """
        with self._dbcontext() as _db:
            try:
                if self._dbEnv:
                    # Wrap this in a transaction snapshot so we can
                    # still write to the database even with the
                    # cursors open
                    _dbTxn = self._dbEnv.txn_begin(flags=db.DB_TXN_SNAPSHOT)
                    self._txnSet.add(_dbTxn)
                else:
                    _dbTxn = None
                _dbC = [_db.cursor(txn=_dbTxn,flags=self._db_cursor_flags)]
                for _,k in keys:  # k is the keystring
                    _dbC.append(\
                        self._db2[k].cursor(txn=_dbTxn,flags=self._db2_cursor_flags))
                    self._debug('cursor context yielding {}'.format(_dbC))
                self._cursorLock |= set(_dbC)
                yield _dbC
            except Exception as err:
                self._error('exception in cursor context: {}'.format(err))
                #raise
            finally:
                for c in _dbC:
                    try:
                        self._debug('closing cursor {}'.format(c))
                        c.close()
                    except:
                        self._error("exception closing cursor {}".format(c))
                self._cursorLock -= set(_dbC)
                if self._dbEnv:
                    self._txnSet.remove(_dbTxn)
                    _dbTxn.commit()
                    self._debug('cursor transaction committed')

    @contextmanager
    def _joincontext(self, curslist):
        """ Safely create a join cursor """

        ## We assume that we are in a _dbcontext already
        if curslist:
            join = self._db.join(curslist)
            self._cursorLock.add(join)
        else:
            join = None
        self._debug('join context yielding {}'.format(join))
        try:
            yield join
        except Exception as err:
            self._error('exception in join context: {}'.format(err))
           # raise
        finally:
            if curslist:
                self._debug("closing join cursor...")
                join.close()
                self._cursorLock.remove(join)

    def _debug(self, debugstring):
        if self._debugmessages:
            print(debugstring)

    def _error(self, errstring):
        print(errstring)


def get_buried_elem(d, key):
    """ Get an element from the dict d specified by key
    
    Key can access nested elements by specifying a sequence
    of nested keys. For example key=('a','b','c') will return
    d['a']['b']['c']"""
    if isinstance(key, basestring):
        key = (key,)
    return _recursive_dict_get(d, key)

def _recursive_dict_get(d, k):
    """ Get an element from the dict d specified by iterable of keys k """
    if len(k) == 1:
        return d[k[0]]
    else:
        return _recursive_dict_get(d[k[0]],k[1:])

def _recursive_dict_pop(d, k):
    """ Get and remove the element of dict d specified by iterable of keys k"""
    if (len(k)) == 1:
        return d.pop(k[0])
    else:
        val = _recursive_dict_pop(d[k[0]],k[1:])
        if not d[k[0]]:  #it could be empty now
            del d[k[0]]
        return val

def _recursive_dict_del(d,k):
    """ Remove element of d specified by iterable of keys k"""
    if (len(k)) == 1:
        del d[k[0]]
    else:
        _recursive_dict_del(d[k[0]],k[1:])


def _recursive_dict_map(f, d1):
    """ Map the function f onto the leaves of d1, return result """
    d2 = {}
    _recursive_dict_map_helper(f,d1,d2)
    return d2
    
def _recursive_dict_map_helper(f,d1,d2={}):
    if isinstance(d1, Mapping):
        for k,v in d1.items():
            if isinstance(v, Mapping):
                d3 = {}
                _recursive_dict_map_helper(f,v,d3)
                if d3:
                    d2[k] = d3
            elif v:
                d2[k] = f(v)
        
def _recursive_dict_keys(d):
    """ Return a list of keys suitable for use in _recursive_dict_get"""
    keys = []
    _recursive_dict_keys_helper(d,[],keys)
    return keys

def _recursive_dict_keys_helper(d,thiskey,keylist):
    if isinstance(d, Mapping):
        return [_recursive_dict_keys_helper(v,thiskey+[k],keylist)
                for k,v in d.items()]
        #print [(v,l+[k]) for k,v in d.viewitems()]
    else:
        keylist.append(tuple(thiskey))

## t1 = {'a' : 1, 'b': 2, 'g' :5}
## t2 = {'c' : 3, 'd' : t1.copy(), 'e' : 4}
## t1['f'] = 6
## t2['h'] = t1.copy()
## t2['d']['i'] = {'m' : 9 }
## k1 = ('a' , {'d' : ('a', 'b'), 'h' : 'f'}, 'e')
## k2 = ('a' , {'d' : ('a', 'i')}, {'h' : 'f'}, 'e')

def _recursive_dict_get_subset(tv, d2):
    """ Returns the subset of dict d2 specified by values tv

    tv is a iterator-tree structure with leaves of  strings, e.g.
    ('1', {'2' : ('2_1', '2_3'), '3' : '3_1'}, ...)
    """

    d3 = {}
    _recursive_dict_get_subset_helper(tv, d2, d3)
    return d3

def _recursive_dict_get_subset_helper(tv, d2, d3 = {}):
    if isinstance(tv, basestring):  # This is just a key
        if tv in d2:
            d3[tv] = d2[tv]
    elif isinstance(tv, Mapping): # Get ready to go down a level
        for k,v in tv.iteritems():
            if k in d2:
                d4 = {}
                _recursive_dict_get_subset_helper(v, d2[k], d4)
                d3[k] = d4
    else:  # 1-D iterable of strings and/or dictionaries, hopefully 
        for k in tv:
            _recursive_dict_get_subset_helper(k, d2, d3)

def _is_map_or_mutable_iterable(thing):
    return isinstance(thing, (MutableSet, MutableSequence, Mapping))

def _recursive_dict_subset(d1, d2):
    """Determine if dict d1 is a subset of dict d2

    Note that a mutable sequence or set leaf in d1 IS a subset of a
    dict at the same position on d2 if keys in d2 correspond to
    elements of the sequence or set on d1. Non-mutable iterables are
    considered monolithic and are compared using ==

    d2 = {'a' : [{'b' : 'c', 'f' : 'g'},{'i': ['j','k','l']}]}
    d1 = {'a' : [{'b' : 'c'},{'i':['j']}]}
    _recursive_dict_subset(d1,d2)
    ==> True
    """
    for k,v in d1.viewitems():
        if k not in d2:
            return False
        else:
            try:
                # If these are dict-like, recurse down
                if isinstance(v, Mapping):
                    if not _recursive_dict_subset(v,d2[k]):
                        return False
                # If they are not dicts, could still be a list of dicts
                elif _is_map_or_mutable_iterable(v):
                    # break elements of v, d2[k] into maps and non-maps
                    v_not_map = [m for m in v if not isinstance(m, Mapping)]
                    d2k_not_map = [m for m in d2[k] if not isinstance(m, Mapping)]
                    if not set(v_not_map) <= set(d2k_not_map):
                        return False
                    vmap = [m for m in v if isinstance(m, Mapping)]
                    d2kmap = [m for m in d2[k] if isinstance(m, Mapping)]
                    for i in vmap:
                        # each map in v needs to be a subset of at
                        # least 1 map in d2k. this is expensive to
                        # search because these things aren't ordered
                        found_subset = False
                        for j in d2kmap:
                            found_subset = _recursive_dict_subset(i,j) \
                                           or found_subset
                        if not found_subset:
                            return False
                elif isinstance(v, np.ndarray):
                    if not np.array_equal(v, d2[k]):
                        return False
                elif v != d2[k]:
                    return False
            except (ValueError, TypeError):
                #print('key: {}\nval: {}\ntype v: {}\ntype d2[k]: {}'
                #     .format(k,v,type(v),type(d2[k])))
                return False
    # We made it this far: we have a match
    return True
