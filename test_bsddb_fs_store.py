from bsddb_fs_store import *
import numpy as np

basic = True
query = True
index = True
addmore = True
dbenv = True
inprogress = True
select = True


init_store('testdb')
if dbenv:
    environment = DBFileSystemEnvironment('testdb',recover=False)
else:
    environment=None
    
store = DBFileSystemStore('testdb','test.db',environment)
#store._debugmessages = True
store2 = DBFileSystemStore('testdb','test2.db',environment)

if index:
    for idx in list(store.get_indices()):
        print('deleting index {}'.format(idx))
        store.destroy_index(idx)
    for idx2 in list(store2.get_indices()):
        print('deleting index {}'.format(idx2))
        store2.destroy_index(idx2)
    

if basic:

    d1 = {'data1' : np.arange(1,12),\
         'data2' : np.ones((2,2,2)) * np.arange(0,2),\
         'data3' : range(10),\
         'data4' : {'data4a' : np.zeros((2,2)),\
                    'data4b' : np.arange(3)}}

    d2 = d1.copy()
    d2['data2'] = np.zeros((2,2,2))
    del d2['data1']
    d2['data3'] = range(20)
    d2['data5'] = 1

    md1 = {'size': 'big',\
          'tuple' : (1,2,3), \
          'list' : [1,2,3,4], \
          'dict' : {'nested' : np.arange(5)}}

    md2 = md1.copy()
    md2['size'] = 'small'
    md2['new'] = ('sting','bee')
    del md2['tuple']

    print "connected: %s" % store.is_connected()
    store.connect()
    print "connected: %s" % store.is_connected()
    print "info: %s" % store.info()

    print "deleting key: 0"
    store.delete('0')

    try:
        print "Getting whatever is in key: 0"
        store.get('0')
    except KeyError as err:
        print "Key Error: %s" % str(err)

    print "setting data=None, md=%s" % md1
    store.set('0', (None, md1))
    store2.set('0', (None, md1))    

    print store.get('0')

    print "setting data=d1, md=md2"

    store.set('0', (d1, md2))
    store2.set('0', (d1, md2))

    print store.get('0')
    print store2.get('0')

    print "disconnecting (uses context manager now)"
    store.disconnect()
    print "connected: %s" % store.is_connected()

    print "updating metadata with md1"
    store.update_metadata('0', md1)

    print store.get('0')

    print 'setting data to d2'
    store.set_data('0', d2)

    print store.get('0')

    print "setting key: 1 to (d1,md2)"

    store.set('1', (d1, md2))

    print "disconnecting"
    store.disconnect()
    print "connected: %s" % store.is_connected()
    print "connecting"
    store.connect()
    print "connected: %s" % store.is_connected()

    print "getting (data,metadata) at key: 1"
    print store.get('1')

    print "getting (data,metadata) at key: 0"
    print store.get("0")
    
if query:
 
    print 'querying for everything'
    for r in store.query():
        print r

    print 'querying for size = big'
    for r in store.query(size='big'):
        print r

    print "done"

if index:
    store.create_index('size')

if addmore:
    md3 = {'size': 'small',\
          'tuple' : (1,5,3), \
          'dict' : {'nested' : np.arange(5)},\
          'anchor' : 1,\
          'list' : [2,4,6]}
    store.set('2',(None,md3))
    store.set('3',(None,md2))
    md4 = {'size': 'big',\
           'anchor' : 'away',\
           'tuple' : (2,2,2)}
    md5 = {'size': 'small',\
           'anchor' : 'away',\
           'tuple' : (2,4,2)}
    md6 = {'size' : 'elephantine',\
           'anchor' : 'in',\
           'tuple' : (2,8,9),\
           'list' : [3,1,5,3]}
    store.set('4',(None,md4))
    store.set('5',(None,md5))
    store.set('6',(d1, md6))

if query:
    print 'querying for everything'
    for r in store.query():
        print r

    print 'querying for size = big, anchor=away'
    for r in store.query(size='big',anchor='away'):
        print r

    print 'querying for size = starge, anchor=away'
    for r in store.query(size='starge',anchor='away'):
        print r

    print 'query for size = small, dict={nested: np.arange(5)}'
    for r in store.query(size='small',dict={'nested':np.arange(5)}):
        print r
if index:
    store.create_index(('dict','nested'))
    store.create_index('tuple')
    store.create_index('list')

store.disconnect()

if query:
    print 'querying for everything'
    for r in store.query():
        print r

    print 'querying for size = big, anchor=away'
    for r in store.query(size='big',anchor='away'):
        print r

    print 'querying for tuple=(2,)'
    for r in store.query(tuple=(2,)):
        print r

    print 'querying for tuple=(2,4,2)'
    for r in store.query(tuple=(2,4,2)):
        print r

    print 'querying for list=[]'
    for r in store.query(list=[]):
        print r

    print 'querying for list=[3]'
    for r in store.query(list=[3]):
        print r
    print 'querying for size = starge, anchor=away'
    for r in store.query(size='elephantine',anchor='away'):
        print r

    print 'query for size = small, dict={nested: np.arange(5)}'
    q = store.query(size='small')
    e = q.next()
    print e

if inprogress:
    print 'with query in progress, pulling key 4'
    print store.get('4')

    print 'with query in progress, writing to key 4!'
    store.set_metadata('4', md3)

    print 'with query in progress, deleting key {}  with cursor'.format(e[0])
    store.delete(e[0])

    print 'with query in progress, a second query!'
    for r in store.query(size='small', anchor='away'):
        print r
if select:
    print 'query for size=small, select= ["size","pig", {"dict": "nested"}]'
    for r in store.query(select=["size","pig", {"dict": "nested"}],size='small'):
        print r

if index:
    try:
        store.destroy_index(('dict','nested'))
    except DBFileSystemLockingError as err:
        print(err)
        print('closing query')
        q.close()
    store.destroy_index(('dict','nested'))
    
